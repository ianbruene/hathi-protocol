
#
# Makefile for hathi-protocol
#

build:
	go build mock/dial-mocker.go
test:
	go test ./...
govet:
	go vet ./...
gofmt goformat:
	go fmt ./...
#golint:
