package protocol

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"reflect"
	"testing"
)

// === struct/map converters === //

func TestParseMap(t *testing.T) {
	// Test working
	pktMap := map[string]interface{}{"version": 1, "request": 2, "function": "do",
		"actor": "foo/man/chu", "error": "none", "many": "few",
		"session": "ffeeddccbbaa99887766554433221100", "object-id": "baz",
		"object":   map[string]interface{}{"one": 1},
		"username": "1337user", "password": "pass123"}
	pkt, err := ParseMap(pktMap)
	if err != nil {
		t.Fatal("ParseMap error:", err)
	}
	if pkt.Version != 1 {
		t.Fatal("ParseMap failure:", pkt.Version)
	} else if pkt.RequestID != 2 {
		t.Fatal("ParseMap failure:", pkt.RequestID)
	} else if pkt.Function != "do" {
		t.Fatal("ParseMap failure:", pkt.Function)
	} else if pkt.Actor != "foo/man/chu" {
		t.Fatal("ParseMap failure:", pkt.Actor)
	} else if pkt.Error != "none" {
		t.Fatal("ParseMap failure:", pkt.Error)
	} else if pkt.ObjectID != "baz" {
		t.Fatal("ParseMap failure:", pkt.ObjectID)
	} else if pkt.Username != "1337user" {
		t.Fatal("ParseMap failure:", pkt.Username)
	} else if pkt.Password != "pass123" {
		t.Fatal("ParseMap failure:", pkt.Password)
	} else if reflect.DeepEqual(pkt.SessionID, [16]byte{
		0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88,
		0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00,
	}) != true {
		t.Fatal("ParseMap failure:", pkt.SessionID)
	} else if reflect.DeepEqual(
		pkt.Object, map[string]interface{}{"one": 1},
	) != true {
		t.Fatal("ParseMap failure:", pkt.Object)
	}
	// Test mangled session ID
	pktMap = map[string]interface{}{"version": 1, "session": "thingamajig"}
	pkt, err = ParseMap(pktMap)
	if err == nil || err.Error() != "Unparsable sessionID string" {
		t.Fatal("ParseMap failure:", err)
	}
	if pkt != nil {
		t.Fatal("ParseMap failure:", pkt)
	}
	// Test incorrect session ID length
	pktMap = map[string]interface{}{"version": 1, "session": "fade"}
	pkt, err = ParseMap(pktMap)
	if err == nil || err.Error() != "Incorrect sessionID length" {
		t.Fatal("ParseMap failure:", err)
	}
	if pkt != nil {
		t.Fatal("ParseMap failure:", pkt)
	}
	// Test no version
	pktMap = map[string]interface{}{"foo": 1}
	pkt, err = ParseMap(pktMap)
	if err == nil || err.Error() != "Missing version field" {
		t.Fatal("ParseMap failure:", err)
	}
	if pkt != nil {
		t.Fatal("ParseMap failure:", pkt)
	}
}

func TestMapify(t *testing.T) {
	pkt := Packet{
		Version:   1,
		RequestID: 2,
		SessionID: [16]byte{
			0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88,
			0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00},
		Function:    "do",
		Actor:       "foo/man/chu",
		ObjectID:    "baz",
		Object:      map[string]interface{}{"one": 1},
		Error:       "none",
		Username:    "1337user",
		Password:    "pass123",
		ExtraFields: map[string]interface{}{"many": "few"},
	}
	pktMap := pkt.Mapify()
	if len(pktMap) != 11 {
		t.Fatal("Mapify failure:", pktMap)
	} else if pktMap["version"].(int) != 1 {
		t.Fatal("Mapify failure:", pktMap["version"].(int))
	} else if pktMap["request"].(int) != 2 {
		t.Fatal("Mapify failure:", pktMap["request"].(int))
	} else if pktMap["function"].(string) != "do" {
		t.Fatal("Mapify failure:", pktMap["function"].(string))
	} else if pktMap["error"].(string) != "none" {
		t.Fatal("Mapify failure:", pktMap["error"].(string))
	} else if pktMap["session"].(string) != "ffeeddccbbaa99887766554433221100" {
		t.Fatal("Mapify failure:", pktMap["session"].(string))
	} else if pktMap["actor"].(string) != "foo/man/chu" {
		t.Fatal("Mapify failure:", pktMap["actor"].(string))
	} else if pktMap["object-id"].(string) != "baz" {
		t.Fatal("Mapify failure:", pktMap["object-id"].(string))
	} else if pktMap["username"].(string) != "1337user" {
		t.Fatal("Mapify failure:", pktMap["username"].(string))
	} else if pktMap["password"].(string) != "pass123" {
		t.Fatal("Mapify failure:", pktMap["password"].(string))
	} else if pktMap["many"].(string) != "few" {
		t.Fatal("Mapify failure:", pktMap["many"].(string))
	} else if reflect.DeepEqual(pktMap["object"].(map[string]interface{}),
		map[string]interface{}{"one": 1},
	) != true {
		t.Fatal("Mapify failure:", pktMap["object"].(map[string]interface{}))
	}
}

// === map/json converters === //

func TestSerializeMap(t *testing.T) {
	m := map[string]interface{}{"one": 1, "foo": "bar"}
	data, err := SerializeMap(m)
	if err != nil {
		t.Fatal("SerializeMap error:", err)
	}
	dataStr := string(data)
	if (dataStr != `{"one":1,"foo":"bar"}`) &&
		(dataStr != `{"foo":"bar","one":1}`) {
		t.Fatal("SerializeMap failure:", dataStr)
	}
}

func TestParseJSON(t *testing.T) {
	data := []byte(`{"one":1,"foo":"bar"}`)
	obj, err := ParseJSON(data)
	if err != nil {
		t.Fatal("ParseJSON error:", err)
	}
	if len(obj) != 2 ||
		obj["one"].(float64) != 1 ||
		obj["foo"].(string) != "bar" {
		t.Fatal("ParseJSON failure:", obj)
	}
}

// === all in one converters === //

func TestParsePacket(t *testing.T) {
	pktData := []byte(`{"version":1,"actor":"foo","request":1,"function":"a"}`)
	pkt, err := ParsePacket(pktData)
	if err != nil {
		t.Fatal("ParsePacket error:", err)
	}
	if pkt == nil {
		t.Fatal("ParsePacket failure:", pkt)
	} else if pkt.Version != 1 {
		t.Fatal("ParsePacket failure:", pkt.Version)
	} else if pkt.Actor != "foo" {
		t.Fatal("ParsePacket failure:", pkt.Actor)
	} else if pkt.RequestID != 1 {
		t.Fatal("ParsePacket failure:", pkt.RequestID)
	} else if pkt.Function != "a" {
		t.Fatal("ParsePacket failure:", pkt.Function)
	}
}

func TestSerializePacket(t *testing.T) {
	p := &Packet{Version: 1, Actor: "foo"}
	data, err := p.SerializePacket()
	if err != nil {
		t.Fatal("SerializePacket error:", err)
	}
	dataStr := string(data)
	if (dataStr != `{"version":1,"actor":"foo"}`) &&
		(dataStr != `{"actor":"foo","version":1}`) {
		t.Fatal("SerializePacket failure:", dataStr)
	}
}
