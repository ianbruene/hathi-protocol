package protocol

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"encoding/hex"
	"encoding/json"
	"errors"
	//"fmt"
)

// Field keys
const (
	f_version      = "version"
	f_requestID    = "request"
	f_sessionID    = "session"
	f_functionName = "function"
	f_actorID      = "actor"
	f_objectID     = "object-id"
	f_object       = "object"
	f_error        = "error"
	f_uname        = "username"
	f_passw        = "password"
)

type Packet struct {
	Version     int
	RequestID   int
	SessionID   [16]byte
	Function    string
	Actor       string
	ObjectID    string
	Object      map[string]interface{}
	Error       string
	Username    string
	Password    string
	ExtraFields map[string]interface{} // for cross version compatability
}

// ==== struct / map conversions ===== //

func ParseMap(pktMap map[string]interface{}) (p *Packet, err error) {
	p = new(Packet)
	p.ExtraFields = make(map[string]interface{}, 0)
	foundVersion := false
	foundRequest := false
	foundFunction := false
	for key, value := range pktMap {
		switch key {
		case f_version:
			v, is := valueAsInt(value)
			if is {
				p.Version = v
				foundVersion = true
			}
		case f_requestID:
			v, is := valueAsInt(value)
			if is {
				p.RequestID = v
				foundRequest = true
			}
		case f_sessionID:
			v, is := valueAsString(value)
			if is {
				decoded, err := hex.DecodeString(v)
				if err != nil {
					return nil, errors.New("Unparsable sessionID string")
				}
				sidLen := len(decoded)
				if sidLen == 16 {
					for i, b := range decoded {
						p.SessionID[i] = b
					}
				} else if sidLen == 0 {
					for i, _ := range p.SessionID {
						p.SessionID[i] = 0
					}
				} else {
					return nil, errors.New("Incorrect sessionID length")
				}
			}
		case f_functionName:
			v, is := valueAsString(value)
			if is {
				p.Function = v
				foundFunction = true
			}
		case f_actorID:
			v, is := valueAsString(value)
			if is {
				p.Actor = v
			}
		case f_objectID:
			v, is := valueAsString(value)
			if is {
				p.ObjectID = v
			}
		case f_object:
			v, is := valueAsObjectMap(value)
			if is {
				p.Object = v
			}
		case f_error:
			v, is := valueAsString(value)
			if is {
				p.Error = v
			}
		case f_uname:
			v, is := valueAsString(value)
			if is {
				p.Username = v
			}
		case f_passw: // TODO: does this need to be hashed here?
			v, is := valueAsString(value)
			if is {
				p.Password = v
			}
		default:
			p.ExtraFields[key] = value
		}
	}
	// Check for required fields
	if foundVersion == false {
		return nil, errors.New("Missing version field")
	} else if foundFunction == false {
		return nil, errors.New("Missing function field")
	} else if foundRequest == false {
		return nil, errors.New("Missing request field")
	}
	return p, nil
}

func (p *Packet) Mapify() (pktMap map[string]interface{}) {
	// dump existing fields to map
	pktMap = make(map[string]interface{}, 0)
	pktMap[f_version] = p.Version
	if p.RequestID != 0 {
		pktMap[f_requestID] = p.RequestID
	}
	hexSession := hex.EncodeToString(p.SessionID[:])
	if hexSession != "00000000000000000000000000000000" {
		pktMap[f_sessionID] = hex.EncodeToString(p.SessionID[:])
	}
	if p.Function != "" {
		pktMap[f_functionName] = p.Function
	}
	if p.Actor != "" {
		pktMap[f_actorID] = p.Actor
	}
	if p.Error != "" {
		pktMap[f_error] = p.Error
	}
	if p.ObjectID != "" {
		pktMap[f_objectID] = p.ObjectID
	}
	if p.Object != nil {
		pktMap[f_object] = p.Object
	}
	if p.Username != "" {
		pktMap[f_uname] = p.Username
	}
	if p.Password != "" {
		pktMap[f_passw] = p.Password
	}
	// extra fields
	for name, value := range p.ExtraFields {
		pktMap[name] = value
	}
	return pktMap
}

// ===== map / json conversions ===== //

func SerializeMap(obj map[string]interface{}) (data []byte, err error) {
	data, err = json.Marshal(obj)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func ParseJSON(data []byte) (obj map[string]interface{}, err error) {
	var tmp interface{}
	err = json.Unmarshal(data, &tmp)
	if err != nil {
		return nil, err
	}
	obj = tmp.(map[string]interface{})
	return obj, nil
}

// ===== All-in-one parser functions ===== //

func ParsePacket(data []byte) (p *Packet, err error) {
	objMap, err := ParseJSON(data)
	if err != nil {
		return nil, err
	}
	p, err = ParseMap(objMap)
	if err != nil {
		return nil, err
	}
	return p, nil
}

func (p *Packet) SerializePacket() (data []byte, err error) {
	objMap := p.Mapify()
	data, err = SerializeMap(objMap)
	if err != nil {
		return nil, err
	}
	return data, nil
}

// ===== Utilities ===== //

func valueAsObjectMap(value interface{}) (obj map[string]interface{}, isObj bool) {
	switch typedV := value.(type) {
	case map[string]interface{}:
		return typedV, true
	default:
		return nil, false
	}
}

func valueAsInt(value interface{}) (integer int, isInt bool) {
	switch typedV := value.(type) {
	case uint:
		return int(typedV), true
	case int:
		return typedV, true
	case float64:
		return int(typedV), true
	default:
		return 0, false
	}
}

func valueAsString(value interface{}) (str string, isString bool) {
	switch typedV := value.(type) {
	case string:
		return typedV, true
	default:
		return "", false
	}
}
